( function(){
    'use strict';

    var fbinhouse = {
        isIeDetected: false,

        isMobile: function(){
            if(( typeof window.matchMedia !== 'undefined' || typeof window.msMatchMedia !== 'undefined' ) && window.matchMedia( '( max-width: 480px )' ).matches ) {
                return true;
            }

            return false;
        },

        initResponsiveImages: function( $ ){
            $( 'img[data-responsive-img]' ).each( function(){
                $( this ).attr( 'src', fbinhouse.baseUrl + $( this ).data( 'responsive-img' ));
            });
        },

        isIpad: function(){
            return navigator.userAgent.match( /ipad/gim );
        },

        isIOS: function(){
            return navigator.userAgent.match( /(iPad|iPhone|iPod)/g );
        },

        isOldIE: function( $ ){
            if( !fbinhouse.isIeDetected ){
                fbinhouse.detectIe( $ );
            }

            return $( 'html' ).is( '.oldie' );
        },

        isIE: function( $, version ){
            if( !fbinhouse.isIeDetected ){
                fbinhouse.detectIe( $ );
            }

            return $( 'html' ).is( '.ie' + version );
        },

        detectIe: function( $ ){
            if( navigator && navigator.appName && navigator.appName.indexOf( 'Internet Explorer' ) > -1 ) {
                if( navigator.appVersion.indexOf( 'MSIE 9.' ) > 1 ) {
                    $( 'html' ).addClass( 'ie9' );
                } else if( navigator.appVersion.indexOf( 'MSIE 8.' ) > 1 ) {
                    $( 'html' ).addClass( 'ie8 oldie' );
                } else if( navigator.appVersion.indexOf( 'MSIE 7.' ) > 1 ) {
                    $( 'html' ).addClass( 'ie7 oldie' );
                } else if( navigator.appVersion.indexOf( 'MSIE 6.' ) > 1 ) {
                    $( 'html' ).addClass( 'ie6 oldie' );
                }
            }

            fbinhouse.isIeDetected = true;
        }
    };

    window.fbinhouse = fbinhouse;

})();

fbinhouse.projectName = 'vcs-sitecore-template';

fbinhouse.client = {
    name : 'volvo',
    site : 'volvocars.com',
    host : 'vcs.fbinhouse.se'
};

if( window.location.href.indexOf( 'localhost' ) > -1 ){
    // Localhost
    fbinhouse.baseUrl = 'http://localhost:8888/' + fbinhouse.projectName + '/www/';
} else if( window.location.href.match( /10\.0\.\d{1,3}\.\d{1,3}/ )){
    // Preview on our internal IP adresses
    fbinhouse.baseUrl = 'http://' + window.location.host + '/' + fbinhouse.projectName + '/www/';
} else if( window.location.href.indexOf( 'hector.fbinhouse.se' ) > -1 ){
    // Dynamic match of devil versions
    if( window.location.href.indexOf( '/develop/' ) > -1 ){
        // Dynamic match of release versions
        fbinhouse.baseUrl = 'http://hector.fbinhouse.se/' + fbinhouse.projectName + '/develop/';
    } else if( window.location.href.indexOf( '/release/' ) > -1 ){
        // Dynamic match of release versions
        fbinhouse.baseUrl = 'http://hector.fbinhouse.se/' + fbinhouse.projectName + '/release/' + window.location.href.match( /\d+\.\d+\.\d+/ )[ 0 ] + '/';
    } else if( window.location.href.indexOf( '/hotfix/' ) > -1 ){
        // Dynamic match of hotfix versions
        fbinhouse.baseUrl = 'http://hector.fbinhouse.se/' + fbinhouse.projectName + '/hotfix/' + window.location.href.match( /\d+\.\d+\.\d+/ )[ 0 ] + '/';
    } else {
        // Match of feature versions
        fbinhouse.baseUrl = 'http://hector.fbinhouse.se/' + fbinhouse.projectName + '/feature/' + window.location.href.match( /feature\/(.+?)\// )[ 1 ] + '/';
    }
} else {
    fbinhouse.baseUrl = '//' + fbinhouse.client.host + '/' + fbinhouse.projectName + '/';
}

// Use \x3C instead of < to ensure good behaviour in older browsers and XHTML strict mode
// http://stackoverflow.com/questions/236073/why-split-the-script-tag-when-writing-it-with-document-write

/****** Needed libs for this to work ******/
//document.write( '\x3Cscript src="//cdn.jsdelivr.net/g/es5.shim@4.1.14,jquery@1.11.3,velocity@1.2.3(velocity.min.js+velocity.ui.min.js),underscorejs@1.8.3,imagesloaded@4.1.0">\x3C/script>' );

// We need to load this in a separate file to make sure the libs have loaded
//document.write( '\x3Cscript src="' + fbinhouse.baseUrl + 'scripts/merge.min.js">\x3C/script>' );
