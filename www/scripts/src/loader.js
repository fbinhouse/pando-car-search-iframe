fbinhouse.$ = $.noConflict();
jQuery = $;

( function( $ ){
    'use strict';
    var retry = 100;
    var initiated = false;

    setTimeout( function(){
        var inter = setInterval( function(){
            if( $( 'body' ) && $( 'body' ).length ){
                clearInterval( inter );
                $( 'body' ).before( fbinhouse.pageContent );
                if( !initiated && fbinhouse.init ){
                    initiated = true;
                    fbinhouse.init( $ );
                }
            }
        }, retry );
    }, 0 );

})( fbinhouse.$ );
