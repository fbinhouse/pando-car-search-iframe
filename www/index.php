<?php

    define('MSG_NO_ACCESS', 'No access');

    $acceptedDomains = array('localhost');
    $referer = false;
    if (isset($_SERVER['HTTP_REFERER'])) {
        $referer=get_domain($_SERVER['HTTP_REFERER']);
    }

    if (!$referer || !in_array($referer, $acceptedDomains)) {
        header('HTTP/1.0 403 Forbidden');
        exit(MSG_NO_ACCESS);
    }

    function get_domain($url) {
      $pieces = parse_url($url);
      if ($pieces === FALSE) {
          return false;
      } else {
          if (array_key_exists('host', $pieces)) {
              return $pieces['host'];
          } else {
              return false;
          }
      }
    }


    error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);

    $templateUrl = 'test.html';
    $targetMarkup = '</head>';

    $fullPage = file_get_contents($templateUrl);

    $fullPage = str_ireplace(
        $targetMarkup,
        '<script src="scripts/fbinhouse-injector.dev.js"></script>' . "\n" . $targetMarkup,
        $fullPage
    );

    echo $fullPage;
